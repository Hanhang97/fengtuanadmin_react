const CracoLessPlugin = require("craco-less");

/* craco.config.js */
module.exports = {
  // ...
  babel: {
    plugins: [["import", { libraryName: "antd", style: "css" }]],
  },

  plugins: [
    {
      plugin: CracoLessPlugin,
    },
  ],
};

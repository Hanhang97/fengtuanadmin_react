// 导入库
import { lazy } from "react";
import { Navigate } from "react-router-dom";

// 路由懒加载
import lazyload from "./lazyload";

// 导入组件
const Layout = lazy(() => import("../layout"));
const Login = lazy(() => import("../pages/login"));
const Welcome = lazy(() => import("../pages/welcome"));
const Err404 = lazy(() => import("../components/err/404"));
const Err500 = lazy(() => import("../components/err/500"));

// 集中式路由
const routes = [
  { path: "/login", element: lazyload(<Login />) },
  {
    path: "/admin",
    element: lazyload(<Layout />),
    children: [
      { index: true, element: lazyload(<Welcome />) },
      // { path: '路径', element: lazyload(<组件名 />) }
    ],
  },
  { path: "/404", element: lazyload(<Err404 />) },
  { path: "/500", element: lazyload(<Err500 />) },
  { path: "*", element: <Navigate to="/404" /> },
];

export default routes;

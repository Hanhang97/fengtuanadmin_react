// 导入样式
import { GlobalStyle } from "./static/reset";

// 导入UI组件
import { ConfigProvider } from "antd";
import zhCN from "antd/es/locale/zh_CN";

// 导入路由
import { useRoutes } from "react-router-dom";
import routes from "./router";

// 定义组件
function App() {
  return (
    <ConfigProvider locale={zhCN}>
      {/* hello，webopenfather */}
      <GlobalStyle />
      {useRoutes(routes)}
    </ConfigProvider>
  );
}

export default App;

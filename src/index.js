import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

//  导入根组件
import App from "./App";

// 导入仓库
import { Provider } from "react-redux";
import store from "./store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>
);
